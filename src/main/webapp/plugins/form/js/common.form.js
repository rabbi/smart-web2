/**
 * 初始化默认值
 * @param $formObj
 * @param setting
 */
function initDefaultValue($formObj, setting) { //alert("initDefaultValue");
    $formObj.find('.cnoj-sysuser-defvalue,.cnoj-sysdeptname-defvalue,.cnoj-sysorgid-defvalue,.cnoj-sysuserid-defvalue,.cnoj-sysfullname-defvalue').each(function(){
        var $findElement = $(this);
        if(!$findElement.prop('disabled') && utils.isEmpty($findElement.val())) {
            if($findElement.hasClass("cnoj-sysuser-defvalue")) {
                $findElement.val(setting.username);
            } else if($findElement.hasClass("cnoj-sysdeptname-defvalue")) {
                $findElement.val(setting.deptName);
            } else if($findElement.hasClass("cnoj-sysorgid-defvalue")) {
                $findElement.val(setting.orgId);
            } else if($findElement.hasClass("cnoj-sysuserid-defvalue")) {
                $findElement.val(setting.userId);
            } else if($findElement.hasClass("cnoj-sysfullname-defvalue")) {
                $findElement.val(setting.userFullName);
            } else if($findElement.hasClass("cnoj-date-defvalue")) {
                var date = new Date();
                var dateFormat = $findElement.data("date-format");
                $findElement.val(date.Format(dateFormat));
            }else if($findElement.hasClass("cnoj-sysuuid-defvalue")) {
                $findElement.val(setting.uuid);
            }
        }
    });
}

/**
 * 列表控件自动填充监听
 * @param userId 用户ID
 * @param orgId 部门ID
 */
function autoFillListctrlListener(userId, orgId) { //alert("autoFillListctrlListener");
    $(".cnoj-auto-fill-listctrl").each(function () {
        var $this = $(this);
        if($this.hasClass("cnoj-auto-fill-listctrl-listener")) {
            return false;
        }
        //判断是否表单数据ID是否为空或者前缀是否为“NEW_”，如果不为空，则不自动填充；
        var formDataId = $('#form-data-id').val();
        if(utils.isNotEmpty(formDataId) && !formDataId.startWith("NEW_")) {
            return false;
        }
        $this.addClass("cnoj-auto-fill-listctrl-listener");
        //判断是否关联其他字段
        var relateField = $this.data('fill-relate-field');
        if(utils.isNotEmpty(relateField)) {
            var relateFields = relateField.split(',');
            for(var i=0; i<relateFields.length; i++) {
                $("[name=" + relateFields[i] + ']').on('change', function () {
                    var values = new Array(relateFields.length);
                    for(var i=0; i<relateFields.length; i++) {
                        values[i] = $("[name=" + relateFields[i] + ']').val();
                    }
                    _fillValue($this, userId, orgId, values);
                });
            }
        } else {
            _fillValue($this, userId, orgId);
        }
    });

    function _fillValue($element, userId, orgId, values) { //alert("_fillValue");
        var url = $element.data('fill-url');
        if(utils.isEmpty(url)) {
            return false;
        }
        if(utils.isNotEmpty(values)) {
            for(var i=0; i < values.length; i++) {
                var value = values[i];
                if(utils.isEmpty(value)) {
                    value = '';
                }
                url = url.replace('${'+(i+1)+'}', value);
            }
            url = url.replace(/(\$\{\d+\})/g, '');
        }
        url = url.replace('${userId}', userId);
        url = url.replace('${orgId}', orgId);
        var name = $element.attr('name');
        $.get(url, function (response) {
            var $tbody = $element.find("tbody.listctrl-tbody");
            var rows = $tbody.find('tr').length;
            if(response.result == '1') {
                cleanPlugins($element);
                var datas = response.datas;
                for(var i = 0; i < datas.length; i++) {
                    if(i >= rows) {
                        tbAddRow(name, false);
                    }
                    var $row = $tbody.find('tr:eq('+ i +')');
                    for(var j = 0; j < datas[i].length; j++) {
                        var $input = $row.find('input:eq('+(j+1)+')');
                        if($input.length > 0) {
                            //$td.find("input").val(datas[i][j]);
                            $input.val(datas[i][j]);
                            if(utils.isNotEmpty(datas[i][j])) {// alert("触发事件 123"+datas[i][j]);
                                $input.trigger('change');
                                $input.trigger('blur');
                            }
                        }
                    }
                }
                //当添加的行大于返回的数据时，删除多余的行
                var dataLen = datas.length;
                if(rows > dataLen && rows > 1) {
                    for(var i = rows; i > (dataLen - 1); i--) {
                        var $row = $tbody.find('tr:eq('+ i +')');
                        if($row.length > 0) {
                            $row.remove();
                        }
                    }
                    $(".row-sums").trigger('blur');
                    $(".sums").trigger('blur');
                }
            } else {
                for(var i = rows; i > 0; i--) {
                    var $row = $tbody.find('tr:eq('+ i +')');
                    if($row.length > 0) {
                        $row.remove();
                    }
                    $tbody.find('input').val('');
                }
                $(".row-sums").trigger('blur');
                $(".sums").trigger('blur');
            }
            inputPluginEvent($element);
            countRowListener($element);
        });
    }
}

function cleanPlugins($listctrlObj) { //alert("cleanPlugins");
    if(utils.isEmpty($listctrlObj)) {
        return;
    }
    $listctrlObj.find(".cnoj-input-select-listener,.cnoj-input-tree-listener,.cnoj-input-select-relate-listener").each(function () {
        var $this = $(this);
        if($this.hasClass('cnoj-input-select') || $this.hasClass('cnoj-input-select-relate')) {
            $this.inputSelect({destroy: true});
        } else if('cnoj-input-tree') {
            $this.zTreeUtil({isInput: true,destroy: true});
        }
    });
}

/**
 * 行统计监听
 */
function countRowListener($element) { //alert("countRowListener");
    if(utils.isEmpty($element)) {
        $element = $(document);
    }
    $element.find(".cnoj-row-count").each(function () {
        var $this = $(this);
        if ($this.hasClass("cnoj-row-count-listener")) {
            return true;
        }
        $this.addClass("cnoj-row-count-listener");
        var relateField = $this.attr('relate-field');
        var expr = $this.attr('count-expr');
        var $tr = $this.closest('tr');
        if($tr.length > 0 && utils.isNotEmpty(relateField)) {
            var relateFieldArray = relateField.split(',');
            for(var i = 0; i < relateFieldArray.length; i++) {
                var $input = $tr.find("input[name='"+relateFieldArray[i]+"']");
                $input.attr('sort-num', (i+1));
                $input.change(function () {
                    var value = $(this).val();
                    var name = $(this).attr("name");
                    var countExpr = expr;
                    if(!utils.regexNum(value)) {
                        value = 0;
                    }
                    countExpr = countExpr.replace('${'+name+'}', value);
                    var varNames = utils.getVarNames(countExpr);
                    if(varNames.length > 0) {
                        for (var i = 0; i < varNames.length; i++) {
                            var $parent = $this.closest('tr');
                            value = $parent.find('input[name='+varNames[i]+']:eq(0)').val();
                            if(utils.isEmpty(value) || !utils.regexNum(value)) {
                                value = 0;
                            }
                            countExpr = countExpr.replace('${'+varNames[i]+'}', value);
                        }
                    }
                    var count = eval('('+countExpr+')');
                    $this.val(count);
                    $this.trigger('change');
                });
                if(utils.isNotEmpty($input.val())) {
                    $input.trigger('change');
                }
            }
        }
    });
}

/**
 * 初始化计算合计值
 */
function initCalSumValue() { //alert("初始化行统计");
    $(".sums").each(function () {
       var $this = $(this);
       if(utils.isEmpty($this.val()) && !$this.prop('disabled')) {
           $this.trigger('blur');
       }
    });
}