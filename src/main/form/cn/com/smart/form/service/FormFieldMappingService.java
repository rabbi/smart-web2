package cn.com.smart.form.service;

import cn.com.smart.form.bean.entity.TFormFieldMapping;
import cn.com.smart.service.impl.MgrServiceImpl;
import cn.com.smart.web.service.IOPService;
import com.mixsmart.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 乌草坡 2019-09-15
 * @since 1.0
 */
@Service
public class FormFieldMappingService extends MgrServiceImpl<TFormFieldMapping> {

    @Autowired
    private IOPService opServ;

    /**
     * 删除表单映射字段
     * @param formMappingId
     */
    public void deleteByFormMappingId(String formMappingId) {
        if(StringUtils.isEmpty(formMappingId)) {
            return;
        }
        Map<String, Object> param = new HashMap<>(1);
        param.put("formMappingId", formMappingId);
        opServ.execute("delete_field_mapping", param);
    }

}
