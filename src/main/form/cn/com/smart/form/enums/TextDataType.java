package cn.com.smart.form.enums;

import com.mixsmart.utils.StringUtils;

/**
 * 文本数据类型
 * @author lmq
 *
 */
public enum TextDataType {

    /**
     * text -- 普通文本
     */
    TEXT("text"),
    
    /**
     * email -- 邮箱地址
     */
    EMAIL("email"),
    
    /**
     * integer -- 整数
     */
    INTEGER("integer"),
    
    /**
     * num -- 数字（包含小数和整型）
     */
    NUM("num"),
    
    /**
     * decimal -- 小数
     */
    DECIMAL("decimal"),
    
    /**
     * ip -- IP地址
     */
    IP("ip"),
    
    /**
     * phone -- 手机号码
     */
    PHONE("phone"),
    
    /**
     * telephone -- 固定电话号码
     */
    TELEPHONE("telephone"),
    
    /**
     * qq -- QQ号码
     */
    QQ("qq"),
    
    /**
     * chinese -- 中文
     */
    CHINESE("chinese"),
    
    /**
     * idcard -- 身份证号码
     */
    ID_CARD("idcard"),
    
    /**
     * datetime -- 日期时间格式
     */
    DATETIME("datetime"),
    
    /**
     * date -- 日期格式
     */
    DATE("date"),
    
    /**
     * time -- 时间格式
     */
    TIME("time");
    
    private String value;

    private TextDataType(String value) {
        this.value = value;
    }

    /**
     * 获取文本数据类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static TextDataType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        TextDataType type = null;
        for(TextDataType pluginType : TextDataType.values()) {
            if(pluginType.getValue().equals(value)) {
                type = pluginType;
                break;
            }
        }
        return type;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
