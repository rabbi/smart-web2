package cn.com.smart.form.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import cn.com.smart.form.list.bean.AbstractListFieldProp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mixsmart.enums.YesNoType;
import com.mixsmart.utils.StringUtils;

/**
 * 表单列表字段解析抽象类
 * @author lmq
 *
 */
public abstract class AbstractFormListFieldParser implements IFormListFieldParser {

    protected Logger logger;
    
    public AbstractFormListFieldParser() {
        logger = LoggerFactory.getLogger(getClass());
    }

    @Override
    public boolean isSupportListField(Map<String, Object> dataMap) {
        String isListField = StringUtils.handleNull(dataMap.get("is_list_field"));
        YesNoType yesNoType = YesNoType.getSupportDefaultObj(isListField);
        return yesNoType.getValue();
    }

    @Override
    public Collection<AbstractListFieldProp> parseListFields(Map<String, Object> dataMap) {
        Collection<AbstractListFieldProp> listFieldProps = new ArrayList<>(1);
        listFieldProps.add(parseListField(dataMap));
        return listFieldProps;
    }

    /**
     * 解析列表字段
     * @param dataMap
     * @return
     */
    protected abstract AbstractListFieldProp parseListField(Map<String, Object> dataMap);
}
