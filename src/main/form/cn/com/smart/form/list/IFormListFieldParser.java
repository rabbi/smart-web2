package cn.com.smart.form.list;

import java.util.Collection;
import java.util.Map;

import cn.com.smart.form.list.bean.AbstractListFieldProp;

/**
 * 表单列表字段解析接口
 * @author lmq
 *
 */
public interface IFormListFieldParser {

    /**
     * 是否支持列表字段
     * @param dataMap 表单插件字段属性
     * @return 如果支持返回true；否则返回false
     */
    boolean isSupportListField(Map<String, Object> dataMap);
    
    /**
     * 解析列表字段
     * @param dataMap 表单插件字段属性
     * @return 返回解析后的列表字段属性对象
     */
    Collection<AbstractListFieldProp> parseListFields(Map<String, Object> dataMap);
    
}
