package cn.com.smart.form.bean.entity;

import cn.com.smart.bean.DateBean;
import cn.com.smart.bean.LogicalDeleteSupport;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author 乌草坡 2019-09-15
 * @since 1.0
 */
@Entity
@Table(name = "t_form_mapping")
public class TFormMapping extends LogicalDeleteSupport implements DateBean {

    private String id;

    private String formId;

    private String targetFormId;

    private String userId;

    private Date createTime;

    //非持久化属性
    private List<TFormFieldMapping> fieldMappings;

    @Id
    @Column(name = "id", length = 50)
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "form_id", length = 50, nullable = false)
    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    @Column(name = "target_form_id", length = 50, nullable = false)
    public String getTargetFormId() {
        return targetFormId;
    }

    public void setTargetFormId(String targetFormId) {
        this.targetFormId = targetFormId;
    }

    @Column(name = "user_id", length = 50, nullable = false, updatable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", updatable = false)
    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Transient
    @Override
    public String getPrefix() {
        return "FM";
    }

    @Transient
    public List<TFormFieldMapping> getFieldMappings() {
        return fieldMappings;
    }

    public void setFieldMappings(List<TFormFieldMapping> fieldMappings) {
        this.fieldMappings = fieldMappings;
    }
}
